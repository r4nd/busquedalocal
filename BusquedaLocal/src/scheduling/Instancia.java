package scheduling;

public class Instancia
{
	private int _tareas;
	private int _maquinas;
	private double[] _tiempos;
	
	public Instancia(int tareas, int maquinas)
	{
		if( tareas <= 0 )
			throw new IllegalArgumentException("Debe haber al menos una tarea! tareas = " + tareas);
		
		if( maquinas <= 0 )
			throw new IllegalArgumentException("Debe haber al menos una m�quina! maquinas = " + maquinas);
		
		_tareas = tareas;
		_maquinas = maquinas;
		_tiempos = new double[tareas];
	}
	
	public void setTiempo(int tarea, double tiempo)
	{
		if( tarea < 0 || tarea >= _tareas )
			throw new IllegalArgumentException("Se intent� setear el tiempo de una tarea inexistente! tarea = " + tarea);
		
		_tiempos[tarea] = tiempo;
	}
	
	public int getTareas()
	{
		return _tareas;
	}
	
	public int getMaquinas()
	{
		return _maquinas;
	}
	
	public double getTiempo(int tarea)
	{
		if( tarea < 0 || tarea >= _tareas )
			throw new IllegalArgumentException("Se intent� consultar el tiempo de una tarea inexistente! tarea = " + tarea);
		
		return _tiempos[tarea];
	}
}
