package scheduling;

public class Solver
{
	private Instancia _instancia;
	private int _iteraciones;
	
	public Solver(Instancia instancia)
	{
		_instancia = instancia;
	}
	
	public Solucion resolver()
	{
		return busquedaLocal(goloso());
	}
	
	SolucionLocal busquedaLocal(SolucionLocal inicial)
	{
		_iteraciones = 0;
		SolucionLocal solucion = inicial;
		SolucionLocal mejor;
		
		do
		{
			mejor = solucion.mejorVecino();
			if( mejor != null )
			{
				solucion = mejor;
				_iteraciones++;
			}
		}
		while( mejor != null );
		
		return solucion;
	}

	private SolucionLocal goloso()
	{
		throw new RuntimeException("No implementado!");
	}

	public int iteraciones()
	{
		return _iteraciones;
	}
}
