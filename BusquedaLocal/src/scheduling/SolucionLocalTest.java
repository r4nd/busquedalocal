package scheduling;

import static org.junit.Assert.*;

import org.junit.Test;

public class SolucionLocalTest
{
	@Test
	public void mejorVecinoTest()
	{
		SolucionLocal solucion = new SolucionLocal(peque�a());
		solucion.setMaquina(0, 0);
		solucion.setMaquina(1, 0);
		solucion.setMaquina(2, 0);
		solucion.setMaquina(3, 0);
		
		SolucionLocal mejorVecino = solucion.mejorVecino();
		assertEquals(6, mejorVecino.makespan(), 1e-5);
		assertTrue(mejorVecino.getMaquina(3) == 1 ||
				   mejorVecino.getMaquina(3) == 2);
	}
	
	@Test
	public void mejorVecinoDosVecesTest()
	{
		SolucionLocal solucion = new SolucionLocal(peque�a());
		solucion.setMaquina(0, 0);
		solucion.setMaquina(1, 0);
		solucion.setMaquina(2, 0);
		solucion.setMaquina(3, 0);
		
		SolucionLocal mejorVecino = solucion.mejorVecino();
		mejorVecino = mejorVecino.mejorVecino();
		
		assertEquals(4, mejorVecino.makespan(), 1e-5);
	}

	@Test
	public void optimoLocalTest()
	{
		SolucionLocal solucion = new SolucionLocal(peque�a());
		solucion.setMaquina(0, 2);
		solucion.setMaquina(1, 2);
		solucion.setMaquina(2, 1);
		solucion.setMaquina(3, 0);
		
		assertNull(solucion.mejorVecino());
	}
	
	private Instancia peque�a()
	{ 
		Instancia ret = new Instancia(4, 3);
		
		ret.setTiempo(0, 1);
		ret.setTiempo(1, 2);
		ret.setTiempo(2, 3);
		ret.setTiempo(3, 4);
		
		return ret;
	}
}
