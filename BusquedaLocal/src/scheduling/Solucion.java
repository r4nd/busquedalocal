package scheduling;

public class Solucion
{
	private Instancia _instancia;
	private Integer[] _maquinaAsignada;
	
	public Solucion(Instancia instancia)
	{
		_instancia = instancia;
		_maquinaAsignada = new Integer[_instancia.getTareas()];
	}
	
	public Instancia getInstancia()
	{
		return _instancia;
	}
	
	public void setMaquina(int tarea, int maquina)
	{
		if( tarea < 0 || tarea >= _instancia.getTareas())
			throw new IllegalArgumentException("Tarea invalida! tarea = " + tarea);

		_maquinaAsignada[tarea] = maquina;
	}

	public int getMaquina(int tarea)
	{
		if( tarea < 0 || tarea >= _instancia.getTareas())
			throw new IllegalArgumentException("Tarea invalida! tarea = " + tarea);
		
		if( _maquinaAsignada[tarea] == null )
			throw new IllegalArgumentException("Maquina no asignada! tarea = " + tarea);

		return _maquinaAsignada[tarea];
	}

	public double ocupacion(int maquina)
	{
		if( maquina < 0 || maquina >= _instancia.getMaquinas() )
			throw new IllegalArgumentException("Se pidi� la ocupaci�n de una m�quina inexistente! maquina = " + maquina);

		double ret = 0;
		for(int i=0; i<_instancia.getTareas(); ++i)
		{
			if( getMaquina(i) == maquina )
				ret += _instancia.getTiempo(i);
		}
		
		return ret;
	}
	
	public double makespan()
	{
		double ret = Double.MIN_VALUE;
		for(int j=0; j<_instancia.getMaquinas(); ++j)
			ret = Math.max(ret, ocupacion(j));
		
		return ret;
	}
}
