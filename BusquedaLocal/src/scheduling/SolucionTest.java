package scheduling;

import static org.junit.Assert.*;

import org.junit.Test;

public class SolucionTest
{
	@Test(expected = IllegalArgumentException.class)
	public void tareasNoAsignadasTest()
	{
		Instancia inst = instancia();
		Solucion solucion = new Solucion(inst);
		solucion.getMaquina(1);
	}

	@Test
	public void construirSolucionTest()
	{
		Instancia inst = instancia();
		Solucion solucion = new Solucion(inst);
		solucion.setMaquina(0, 2); // Tarea 0 en la m�quina 2
		solucion.setMaquina(1, 0); // Tarea 1 en la m�quina 0
		solucion.setMaquina(4, 1); // Tarea 4 en la m�quina 1
		
		assertEquals(2, solucion.getMaquina(0));
		assertEquals(0, solucion.getMaquina(1));
		assertEquals(1, solucion.getMaquina(4));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void construccionInvalidaNegativaTest()
	{
		Instancia inst = instancia();
		Solucion solucion = new Solucion(inst);
		solucion.setMaquina(-1, 2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void construccionInvalidaExcedidaTest()
	{
		Instancia inst = instancia();
		Solucion solucion = new Solucion(inst);
		solucion.setMaquina(8, 2);
	}

	@Test
	public void tiempoTotalTest()
	{
		Instancia inst = instancia();
		Solucion solucion = solucionDeTest(inst);
		
		assertEquals(5.0, solucion.ocupacion(0), 1e-5);
		assertEquals(9.0, solucion.ocupacion(1), 1e-5);
		assertEquals(4.0, solucion.ocupacion(2), 1e-5);
	}

	@Test
	public void makespanTest()
	{
		Instancia inst = instancia();
		Solucion solucion = solucionDeTest(inst);
		
		assertEquals(9.0, solucion.makespan(), 1e-5);
	}

	@Test
	public void cambioDeMaquinaTest()
	{
		Instancia inst = instancia();
		Solucion solucion = solucionDeTest(inst);
		solucion.setMaquina(0, 0);
		
		assertEquals(0, solucion.getMaquina(0));
		assertEquals(7 , solucion.ocupacion(0), 1e-5);
		assertEquals(2 , solucion.ocupacion(2), 1e-5);
	}

	private Solucion solucionDeTest(Instancia inst)
	{
		Solucion solucion = new Solucion(inst);
		solucion.setMaquina(0, 2);
		solucion.setMaquina(1, 0);
		solucion.setMaquina(2, 0);
		solucion.setMaquina(3, 1);
		solucion.setMaquina(4, 1);
		solucion.setMaquina(5, 1);
		solucion.setMaquina(6, 1);
		solucion.setMaquina(7, 2);
		return solucion;
	}

	private Instancia instancia()
	{
		Instancia ret = new Instancia(8, 3);
		ret.setTiempo(0, 2);
		ret.setTiempo(1, 3);
		ret.setTiempo(2, 2);
		ret.setTiempo(3, 1);
		ret.setTiempo(4, 1);
		ret.setTiempo(5, 4);
		ret.setTiempo(6, 3);
		ret.setTiempo(7, 2);
		
		return ret;
	}
}
