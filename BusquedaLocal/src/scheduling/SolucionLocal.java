package scheduling;

class SolucionLocal extends Solucion
{
	private Instancia _instancia; // mmm ...
	
	public SolucionLocal(Instancia instancia)
	{
		super(instancia);
		_instancia = instancia; // mmm^2
	}

	public SolucionLocal mejorVecino()
	{
		SolucionLocal ret = this;
		
		for(int i=0; i<_instancia.getTareas(); ++i)
		for(int j=0; j<_instancia.getMaquinas(); ++j)
		{
			SolucionLocal vecina = this.clonar();
			vecina.setMaquina(i, j);
			
			if( vecina.makespan() < ret.makespan() )
				ret = vecina;
		}
		
		return ret == this ? null : ret;
	}

	private SolucionLocal clonar()
	{
		SolucionLocal ret = new SolucionLocal(getInstancia());
		
		for(int i=0; i<_instancia.getTareas(); ++i)
			ret.setMaquina(i, this.getMaquina(i));
		
		return ret;
	}
}
