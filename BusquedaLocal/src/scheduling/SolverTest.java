package scheduling;

import static org.junit.Assert.*;

import org.junit.Test;

public class SolverTest
{
	@Test
	public void busquedaLocalTest()
	{
		Instancia instancia = peque�a();
		SolucionLocal solucion = new SolucionLocal(instancia);
		solucion.setMaquina(0, 0);
		solucion.setMaquina(1, 0);
		solucion.setMaquina(2, 0);
		solucion.setMaquina(3, 0);
		
		Solver solver = new Solver(instancia);
		solucion = solver.busquedaLocal(solucion);
		
		assertEquals(4, solucion.makespan(), 1e-5);
		assertEquals(2, solver.iteraciones());
	}
	
	private Instancia peque�a()
	{ 
		Instancia ret = new Instancia(4, 3);
		
		ret.setTiempo(0, 1);
		ret.setTiempo(1, 2);
		ret.setTiempo(2, 3);
		ret.setTiempo(3, 4);
		
		return ret;
	}
}
